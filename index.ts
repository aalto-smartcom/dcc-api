import app from './src/app' // the actual Express application
import http from 'http'
const config = require('./src/utils/config')

const server = http.createServer(app)

server.listen(config.PORT, () => {
  console.log(`Eidas set to connect on ${config.EIDAS_URI}`)
  console.log(`Server running on port http://localhost:${config.PORT}`)
})