# Smartcom Stamping server documentation

This wiki entry contains information about setting up and using the Smartcom Stamping server for sealing digital calibration certificates (DCC) and validating the signature on sealed DCCs. The current proof-of-concept version of the server is provided on an Intel NUC.
If you require any assistance please do not hesitate to contact the development team

* jaan.taponen@aalto.fi Corresponding developer
* leo.immonen@aalto.fi
* nuutti.henriksson@aalto.fi
* henri.tunkkari@aalto.fi

## General information about the Intel NUC

**Model**: BXNUC10i3FNH2

**User guide**: https://www.intel.com/content/dam/support/us/en/documents/mini-pcs/NUC10FNH_UserGuide.pdf

**Default credentials**: *smartcom:smartcom*

**REST API address**: `https://{nuc-ip-address}`

**REST API endpoints**: `/v1/seal`, `v1/validate`

## Setting up the NUC

The NUC acts as a server (called Stamping server) which provides REST API through which the DCC sealing and validation services can be accessed. For the services to be available the NUC must be connected to a network and the user `smartcom` must be logged in:

1. Use an ethernet cable to connect the NUC to a network in which you intend to use it. The NUC has been configured to use `dhcp` so its IP address will depend on your network configuration. The NUC will display its assigned IP address after a user logs in (field `IPv4 address for eno1` in the System information section on the login screen). 

2. To log in to the NUC you can attach it to a monitor and a keyboard and physically log in. Alternatively you can use it in a headless mode over SSH if the NUC's IP address is known before login. The default credentials are `smartcom:smartcom`.

The REST API and logging services become accessible once the user `smartcom` is logged in to the NUC. These services  run in Docker containers which start automatically after the login. Note it may take several minutes until all the Docker containers are up and running after the login. To check whether they are ready, see the section [Checking the status of the services](https://gitlab.com/aalto-smartcom/dcc-api/-/wikis/home#Checking%20the%20status%20of%20the%20services).

In general, the Docker containers should not require additional configuration.

Once all the services are up you can access the REST API at `https://{nuc-ip-address}`. See section [Using the API](https://gitlab.com/aalto-smartcom/dcc-api/-/wikis/home#using-the-api) for examples how to use the REST API.

## Checking the status of the services

You can check whether the services are available by sending a GET request to 

`https://{nuc-ip-address}/dev/status`

which returns a JSON list showing whether the individual services are online or offline. 

Additionally you can query the status of the Docker containers by running

`sudo docker ps` 

on the NUC's command line. This should list six different containers. When the status of the container `elastic-logging_logspout` is `Up` instead of `Restarting`, all the containers are ready. 

## Information about the Docker services

The NUC has two separate stacks that are managed with Docker Compose, `dcc-api` and `elastic-logging`. Their source and configuration files can be found in their respective directories in `/home/smartcom`. `dcc-api` provides the sealing and validation functionalities for DCCs. `elastic-logging` is responsible for logging with the Elastic stack (https://www.elastic.co/elastic-stack) consisting of Elasticsearch, Kibana, Logstash and Logspout. 

All the Docker containers are configured to restart automatically when user `smartcom` logs in or they encounter an error. The standard Docker and Docker Compose commands can be still used to manage the containers if needed. Hence both stack can be brought down with 

`docker-compose down`

when run from their respective folders. They can be brought up again by running 

`docker-compose up`

Note that for Docker Compose stacks to start automatically after a reboot and login, they must be running when the NUC is shutdown. If they are not up during a shutdown, they must be brought up manually after the login. 

When running the API and logging stacks in the same environment, the order in which they are brought up does not matter since Logspout automatically attaches to all running containers in the same environment.

# Using the API

## General usage

The wiki page [SigningAPI specification](https://gitlab.com/aalto-smartcom/dcc-api/-/wikis/SigningAPI-specification) lists the available endpoints and their usage. For specific examples, see section [Signing a DCC with Postman](#Signing a DCC with Postman) below.

Swagger API documentation is also available [in yaml](https://gitlab.com/aalto-smartcom/dcc-api/-/blob/master/documentation/swagger/dcc-spec.yml) and [in html](https://gitlab.com/aalto-smartcom/dcc-api/-/blob/master/documentation/swagger/dcc-spec.html).

## Signing a DCC with Postman

**Set up Postman**

The REST API can be tested easily with [Postman](https://www.postman.com/):

- Open the [Postman collection file](https://gitlab.com/aalto-smartcom/dcc-api/-/blob/master/documentation/postman/signingAPI_postman.json) in Postman by selecting Import and then Upload Files. If you encounter problems, you can also copy the contents of the JSON file to the Raw Text tab in Import window.

- **Change the `api_host` collection variable to match the IP address or hostname of the NUC!** Navigate to the Collections tab on Postman's left-hand panel and open the loaded collection's three-dot menu and click `Edit`. Navigate to the `Variables` tab and change the `Current value` to the NUC's IP address, e.g. `https://192.168.1.2:80`. 
Additional documentation on using variables can be found from Postman's [official documentation](https://learning.postman.com/docs/sending-requests/variables/).

**Signing a DCC – /v1/seal**

The `POST: /v1/seal` endpoint takes the unsigned DCC in XML format as the POST request body (and nothing else). The DCC must conform to the [schema](https://gitlab.com/aalto-smartcom/dcc-api/-/blob/master/templates/schema/) used by SigningAPI. The [Postman collection file](https://gitlab.com/aalto-smartcom/dcc-api/-/blob/master/documentation/postman/signingAPI_postman.json) contains an example DCC which you can sign by clicking the `Send` button. If successful, the response will contain the signed DCC. 

**Make sure you copy the signed DCC in *raw* format from the response in Postman instead of *pretty* format since the latter breaks the signature!** Generally, a valid and signed DCC fails the validation if empty lines, leading whitespaces or trailing whitespaces are added to it after sealing. Removing line breaks or adding comments do not affect the validation.

**Validating a DCC - /v1/validate**

The `POST: /v1/validate` endpoint takes a signed DCC in XML format (as described above). The Postman collection contains an example signed DCC for this endpoint as well.

The response contains conclusion of the validation report. If signature is valid, the conclusion will be "PASSED". Else, information will appear about integrity and status of the signature.
A full validation report can be fetched with query parameter `?verbose=true`. Please refer to official eIDAS dss-demonstrations wiki for documentation about the full validation report.

### Step by step on creating XAdES signatures manually (advanced)

You can find a guide on signing DCCs manually step by step with eIDAS REST [here](https://github.com/AaltoSmartCom/dss-demonstrations/blob/master/documentation/README.md). For that purpose, an another Postman collection for eIDAS can be found [here](https://gitlab.com/aalto-smartcom/dcc-api/-/blob/master/documentation/postman/eIDAS_postman.json).


