import fs from 'fs'
import util from 'util'
import {
  generateFingerprint,
  insertIdentifier,
  removeIdentifier,
  URIPattern
} from '../utils/dccid'
const readFile = util.promisify(fs.readFile)

describe('Test inserting and removing DCC ID URI element', () => {
  test('insertIdentifier -- DCC ID URI element should be found from a signed document after calling insertIdentifier', async () => {
    let signedDCC = await readFile('./templates/tests/xml/signed-dcc.xml', 'utf8')
    signedDCC = await insertIdentifier(signedDCC)

    expect(signedDCC).toMatch(URIPattern)
  })

  test('insertIdentifier -- Trying to insert DCC ID URI to unsigned document, AssertionError should be thrown', async () => {
    let unsignedDCC = await readFile('./templates/tests/xml/dummy-dcc.xml', 'utf8')

    await expect(insertIdentifier(unsignedDCC)).rejects.toThrowError()
  })

  test('removeIdentifier -- DCC ID URI should be removed when calling removeIdentifier', async () => {
    const signedDCCWithId = await readFile('./templates/tests/xml/signed-dcc-with-dccid.xml', 'utf-8')
    expect(signedDCCWithId).toMatch(URIPattern)

    const signedDCC = await removeIdentifier(signedDCCWithId)
    expect(signedDCC).not.toMatch(URIPattern)
  })

  test('removeIdentifier -- when DCC ID URI is not present, nothing should be changed', async () => {
    let signedDCC = await readFile('./templates/tests/xml/signed-dcc.xml', 'utf8')
    const fingerprint = await generateFingerprint(signedDCC)

    signedDCC = await removeIdentifier(signedDCC)
    const fingerprint2 = await generateFingerprint(signedDCC)

    expect(fingerprint2).toEqual(fingerprint)
  })

  test('Fingerprints should match after inserting and removing DCC ID URI (signed document should stay unmodified)', async () => {
    let signedDCC = await readFile('./templates/tests/xml/signed-dcc.xml', 'utf8')
    const fingerprint = await generateFingerprint(signedDCC)

    signedDCC = await insertIdentifier(signedDCC)
    signedDCC = await removeIdentifier(signedDCC)
    const fingerprint2 = await generateFingerprint(signedDCC)

    expect(fingerprint2).toEqual(fingerprint)
  })
})
  