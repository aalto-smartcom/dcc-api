/* eslint-disable quotes */
import supertest from 'supertest'
jest.setTimeout(90000)
import app from '../app'
const api = supertest(app)
import fs from 'fs'
import util from 'util'
const readFile = util.promisify(fs.readFile)
import { ValidationReport, Conclusion } from '../types/validateData'


const checkValidationReport = (validationReport: ValidationReport): boolean => {
  const signatureReport = validationReport.DiagnosticData.Signature[0]
  const structureReport: boolean = signatureReport ? signatureReport.StructuralValidation.Valid : false
  const valid: boolean = signatureReport ?
    signatureReport.BasicSignature.SignatureIntact &&
    signatureReport.BasicSignature.SignatureValid : false
  return structureReport && valid
}

describe('connection test', () => {
  test('Can fetch the html content of root', async () => {
    await api
      .get('/')
      .expect(200)
      .expect('Content-Type', 'text/html; charset=utf-8')
  })
})

describe('Unknown paths should be handled properly and return 404', () => {
  test('GET unknown path at root', async () => {
    await api
      .get('/bruh')
      .expect(404)
  })

  test('GET unknown path at /v1/', async () => {
    await api
      .get('/v1/bruh')
      .expect(404)
  })

  test('GET unknown path at /v1/seal/', async () => {
    await api
      .get('/v1/seal/bruh')
      .expect(404)
  })

  test('GET unknown path at /v1/validate?verbose=true/', async () => {
    await api
      .get('/v1/validate/bruh?verbose=true')
      .expect(404)
  })
})

describe('test sealing', () => {
  test('Well formatted XML with mock data should return a 200 status code', async () => {
    const dcc_correct: string = (await readFile('./templates/tests/xml/dummy-dcc.xml', 'utf8'))
    await api
      .post('/v1/seal')
      .set('Content-Type', 'application/xml')
      .send(dcc_correct)
      .expect(200)
      .expect('Content-Type', 'application/xml; charset=utf-8')
  })
  test('Incorrectly formatted XML should not be accepted', async () => {
    const invalidXml: string = (await readFile('./templates/tests/xml/invalidXML.xml', 'utf8'))
    await api
      .post('/v1/seal')
      .set('Content-Type', 'application/xml')
      .send(invalidXml)
      .expect(400)
  })
  test('DCC lacking mandatory fields should return 400 status code.', async () => {
    const dcc_lacking_field: string = (await readFile('./templates/tests/xml/dcc-lacking-field.xml', 'utf8'))
    await api
      .post('/v1/seal')
      .set('Content-Type', 'application/xml')
      .send(dcc_lacking_field)
      .expect(400)
  })
  test('DCC with incorrect field name should return 400 status code.', async () => {
    const dcc_incorrect_field_name: string = (await readFile('./templates/tests/xml/dcc-incorrect-field-name.xml', 'utf8'))
    await api
      .post('/v1/seal')
      .set('Content-Type', 'application/xml')
      .send(dcc_incorrect_field_name)
      .expect(400)
  })
  test('Test sending invalid filetypes.', async () => {
    await api
      .post('/v1/seal')
      .send('Teststring')
      .expect(400)
    await api
      .post('/v1/seal')
      .send({ 'foo': 'bar' })
      .expect(400)
  })

  test('Test incorrect signeralias', async () => {
    const dcc_correct: string = (await readFile('./templates/tests/xml/dummy-dcc.xml', 'utf8'))
    const invalid  = await api
      .post('/v1/seal')
      .set('signeralias', 'definitelynotfound')
      .set('Content-Type', 'application/xml')
      .send(dcc_correct)
    expect(invalid.status).toBe(400)
    expect(JSON.parse(invalid.text).toString()).toContain({ 'error' : 'Invalid signeralias provided in the request.' })
  })
})

describe('Test validation', () => {
  test('Test validation of signed DCC.', async () => {
    const dcc_signed: string = (await readFile('./templates/tests/xml/signed-dcc-with-dccid.xml', 'utf8'))
    const { body, status } = await api
      .post('/v1/validate?verbose=true')
      .set('Content-Type', 'application/xml')
      .send(dcc_signed)
    expect(status).toBe(200)
    expect(checkValidationReport(body as ValidationReport)).toBe(true)
  })
  test('Test unsigned DCC.', async () => {
    const dcc_correct: string = (await readFile('./templates/tests/xml/dummy-dcc.xml', 'utf8'))
    const { body, status } = await api
      .post('/v1/validate?verbose=true')
      .set('Content-Type', 'application/xml')
      .send(dcc_correct)

    expect(status).toBe(200)
    expect(checkValidationReport(body as ValidationReport)).toBe(false)
  })
  test('Test incorrectly formatted input. JSON (and other invalid inputs) should return 400.', async () => {
    await api
      .post('/v1/validate?verbose=true')
      .send({ 'foo': 'bar' })
      .expect(400)
  })

  test('Test sending only a valid eIDAS signature without DCC.', async () => {
    const eidas_signature: string = (await readFile('./templates/tests/xml/eidas-signature.xml', 'utf8'))
    await api
      .post('/v1/validate?verbose=true')
      .set('Content-Type', 'application/xml')
      .send(eidas_signature)
      .expect(400)
  })
})

// TODO modify the dcc and test validation again
describe('Test Signature and data are intact', () => {
  test('DCC correctly signed and not modified', async () => {
    const dcc_correct: string = (await readFile('./templates/tests/xml/dummy-dcc.xml', 'utf8'))
    const sealed = await api
      .post('/v1/seal')
      .set('Content-Type', 'application/xml')
      .send(dcc_correct)
    expect(sealed.status).toBe(200)
    const signedData = sealed.text

    const timeout = (ms: number) => {
      return new Promise(resolve => setTimeout(resolve, ms))
    }
    await timeout(5000)
    const validated = await api
      .post('/v1/validate?verbose=true')
      .set('Content-Type', 'application/xml')
      .send(signedData)
    expect(validated.status).toBe(200)
    expect(checkValidationReport(validated.body as ValidationReport)).toBe(true)
  })
  
  test('Test full conclusion to be PASSED', async () => {
    const dcc_correct: string = (await readFile('./templates/tests/xml/dummy-dcc.xml', 'utf8'))
    const sealed = await api
      .post('/v1/seal')
      .set('Content-Type', 'application/xml')
      .send(dcc_correct)
    expect(sealed.status).toBe(200)
    const signedData = sealed.text
    const timeout = (ms: number) => {
      return new Promise(resolve => setTimeout(resolve, ms))
    }
    await timeout(5000)
    const validatedNoVerbose = await api
      .post('/v1/validate')
      .set('Content-Type', 'application/xml')
      .send(signedData)
    //console.log((validatedNoVerbose.body as Conclusion).Indication)
    expect((validatedNoVerbose.body as Conclusion).Indication).toBe('PASSED')
  })
})
