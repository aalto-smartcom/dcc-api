let express = require('express')
const app = express()
require('express-async-errors')
import dccRouter from './routes/dccs'
import bodyParser from 'body-parser'
const middleware = require('./utils/middleware')
const morgan = require('morgan')
import cors from 'cors'
const config = require('./utils/config')

app.use(bodyParser.json({limit: '100mb'}));
app.use(bodyParser.text({ type: '*/*', limit: '50mb' }))
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}))//https://stackoverflow.com/questions/38306569/what-does-body-parser-do-with-express
app.use(express.json())
app.use(morgan('dev'))
//
/* const corsoptions: cors.CorsOptions = {
  allowedHeaders: [
    'Origin',
    'X-Requested-With',
    'Content-Type',
    'Accept',
    'X-Access-Token',
    'Cookie',
    'signeralias',
  ],
  credentials: true,
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  origin: config.CORSALLOW,
  preflightContinue: false,
  optionsSuccessStatus: 204
}
app.use(cors(corsoptions)) */
app.use('/', dccRouter)

app.use(middleware.unknownEndpoint)
app.use(middleware.errorHandler)
export default app
