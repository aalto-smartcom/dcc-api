import express from 'express'
import fs from 'fs'
import util from 'util'
const readFile = util.promisify(fs.readFile)
const markdown = require('markdown').markdown
const router = express.Router()
import api_v1 from '../services/api_v1'
import api_dev from '../services/api_dev'
const config = require('../utils/config')

router.post('/v1/validate', async (request, response) => {
  const verbose: boolean = (request.query.verbose||'').toString().toLowerCase() === 'true'
  response.status(200).json(await api_v1.validate(request.body, verbose))
})

router.post('/v1/seal', async (request, response) => {
  response.setHeader('content-type', 'application/xml')
  const disableSchemaValidation  = request.header('disable-schema') !== undefined
  const useralias  = request.header('signeralias') || config.SIGNERALIAS
  response.status(200).send(await api_v1.seal(request.body, useralias, disableSchemaValidation))
})

router.get('/dev/status', async (_request, response) => {
  response.status(200).json(await api_dev.status())
})

router.get('/', async (_request, response) => {
  const instructions = (await readFile('./documentation/instructions.md', 'utf8'))
  response.status(200).send(markdown.toHTML(instructions))
})

export default router
