/* eslint-disable @typescript-eslint/camelcase */
import { DigestResponse, CertificateResponse, ConnectionError, } from '../types/types'
import { GetDataResponse, GetDataRequest } from '../types/getData'
import { SignRequest, SignResponse } from '../types/signData'
import { ValidationRequest, ValidationReport, Conclusion } from '../types/validateData'
import { canonicalizeXML, validateXML, validateXMLWithXSD } from 'super-xmlllint'
import { insertIdentifier, removeIdentifier } from '../utils/dccid'
import axios from 'axios'
import getDataToSign from '../../templates/json/getDataToSign.json'
import signDocument from '../../templates/json/signDocument.json'
import _validateSignature from '../../templates/json/validateSignature.json'

/*  Configuration  */
const config = require('../utils/config')
const XMLSchemaPath = config.XML_SCHEMA_PATH
const canonicalizationMethod = config.CANONICALIZATION_METHOD
const eIDASHost = config.EIDAS_URI

/**
 * Helper functions for Base64 encoding and decoding
 * @param str: string
 */
const decodeB64 = async (str: string): Promise<string> => Buffer.from(str, 'base64').toString('utf8')
const encodeB64 = async (str: string): Promise<string> => Buffer.from(str, 'utf8').toString('base64')


/**
 * Signs the input XML using eIDAS REST API
 * NOTE: Assumes valid XML as input!
 * Returns the signed XML as a string
 * 
 * @param xmlInput: string
 */
const stamp = async (xmlInput: string, signeralias: string): Promise<string> => {

  const signingDate = new Date().getTime()

  const pk12Keys: CertificateResponse = await (await axios
    .get(`${eIDASHost}/services/rest/server-signing/keys`)
    .catch(error => { throw new ConnectionError(error.message) }))
    .data.find((x: { alias: string }) => x.alias === signeralias)

  const pksc12Cert: string = pk12Keys['certificate']['encodedCertificate']

  //Get the data to be signed
  const getDataBody: GetDataRequest = { ...getDataToSign }
  getDataBody.parameters.signingCertificate.encodedCertificate = pksc12Cert
  getDataBody.parameters.certificateChain = pk12Keys.certificateChain
  getDataBody.parameters.blevelParams.signingDate = signingDate
  getDataBody.toSignDocument.bytes = await encodeB64(xmlInput)
  const dataToSign: GetDataResponse = await (await axios
    .post(`${eIDASHost}/services/rest/signature/one-document/getDataToSign`, getDataBody)
    .catch(error => { throw new ConnectionError(error.message) }))
    .data

  //Get the signatureValue (digest)
  const digest: DigestResponse = await (await axios
    .post(`${eIDASHost}/services/rest/server-signing/sign/${signeralias}/SHA256`, dataToSign)
    .catch(error => { throw new ConnectionError(error.message) }))
    .data

  //Stamp the document
  const stampBody: SignRequest = { ...signDocument }
  stampBody.parameters.signingCertificate.encodedCertificate = pksc12Cert
  stampBody.parameters.certificateChain = pk12Keys.certificateChain
  stampBody.parameters.blevelParams.signingDate = signingDate
  stampBody.signatureValue.value = digest.value
  stampBody.toSignDocument.bytes = await encodeB64(xmlInput)
  const signedXML: SignResponse = await (await axios
    .post(`${eIDASHost}/services/rest/signature/one-document/signDocument`, stampBody)
    .catch(error => { throw new ConnectionError(error.message) }))
    .data

  return decodeB64(signedXML.bytes)
}


/**
 * Validates XML input and returns the full validation
 * response from eIDAS service.
 * Returns a ValidationReport JSON object
 * 
 * @param xmlInput
 */
const validateSignature = async (xmlInput: string): Promise<ValidationReport> => {

  const validateBody: ValidationRequest = { ..._validateSignature }
  validateBody.signedDocument.bytes = await encodeB64(xmlInput)
  const response: ValidationReport = await (await axios
    .post(`${eIDASHost}/services/rest/validation/validateSignature`, validateBody)
    .catch(error => {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response.data)
        console.log(error.response.status)
        console.log(error.response.headers)
        throw new ConnectionError(error.response.data)
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log(error.request)
        throw new ConnectionError(error.request)
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message)
        throw new ConnectionError(error.message)
      }
    }))
    .data

  return response
}


/**
 * Takes the input XML as a parameter, then
 * parses, canonicalizes and stamps it
 * Returns canonicalized and signed XML string
 * 
 * @param input The DCC document to be sealed
 * @param useralias
 */
const seal = async (input: string, useralias: string, disableSchemaValidation: boolean): Promise<string> => {

  // Validate the contents as valid XML
  await validateXML(input)

  // Validate the XML against the DCC schema
  if (!disableSchemaValidation) {
    await validateXMLWithXSD(input, XMLSchemaPath)
  }
  

  // Canonicalize the XML
  const canonicalizedXML: string =
    (await canonicalizeXML(input, canonicalizationMethod)).output

  // Sign the XML document and insert DCC ID URI
  const signedXML =  insertIdentifier(await stamp(canonicalizedXML, useralias))

  return signedXML
}


/**
 * Takes the input XML as a parameter, then
 * parses, canonicalizes and validates the signature
 * Returns the full validation response JSON
 * 
 * @param input The DCC document to be validated
 * @param verbose If true, a full ValidationReport is returned, else just a conclusion.
 */
const validate = async (input: string, verbose: boolean = false): Promise<ValidationReport | Conclusion> => {

  // Validate the contents as valid XML
  await validateXML(input)

  // Remove DCC ID URI
  input = await removeIdentifier(input)

  // Canonicalize the XML
  const canonicalizedXML: string =
    (await canonicalizeXML(input, canonicalizationMethod)).output

  // Validate the signature
  const validationReport: ValidationReport = await validateSignature(canonicalizedXML)

  const constructConclusion = () => {
    try {
      return validationReport
        .DetailedReport
        .signatureOrTimestampOrCertificate[0]
        .Signature
        .ValidationProcessBasicSignature
        .Conclusion
    } catch (error) {
      throw new Error('Construction of simple report failed. The most likely cause is a missing signature.')
    }

  }
  return verbose ? validationReport : constructConclusion()
}

export default { validate, seal }
