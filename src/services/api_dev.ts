import axios from 'axios'
import { Service } from '../types/types'

const services: Array<Service> = [
  {
    name: 'eIDAS REST',
    url: 'http://eidas-rest:8080/services/rest/server-signing/keys'
  },
  // {
  //   name: 'Elasticsearch',
  //   url: 'http://elasticsearch:9200'
  // },
  // {
  //   name: 'Kibana',
  //   url: 'http://kibana:5601'
  // },
  {
    name: 'Logstash',
    url: 'http://logstash:9600'
  }
]

const isOnline = (url: string) =>
  axios
    .get(url, { timeout: 10000 })
    .then(() => true)
    .catch(error => 
      ((((error||{}).response||{}).status||{}) === 401) || false)  // Since Elasticsearch requires authentication


const status = async () =>Promise.all(
  services.map(async service =>
    `${service.name}: ${(await isOnline(service.url)) ? 'online' : 'offline'}`
  )
)

export default { status }
