export interface DigestResponse {
    algorithm: string;
    value: string;
}

interface Certificate {
    encodedCertificate: string;
}

interface CertificateChain {
    encodedCertificate: string;
}

export interface CertificateResponse {
    alias: string;
    encryptionAlgo: string;
    certificate: Certificate;
    certificateChain: CertificateChain[];
}

export class ConnectionError extends Error {
  constructor(message: string) {
    super(message)
    this.message = message
    this.name = 'ConnectionError'
  }
}

export interface Service {
    name: string;
    url: string;
}







