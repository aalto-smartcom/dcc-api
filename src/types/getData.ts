
// eslint-disable-next-line no-unused-vars
import { CertificateResponse } from './types'

/***
 * GetDataToSign request types
 */
interface ContentTimestampParameters {
    digestAlgorithm: string;
    canonicalizationMethod: string;
}

interface SignatureTimestampParameters {
    digestAlgorithm: string;
    canonicalizationMethod: string;
}

interface ArchiveTimestampParameters {
    digestAlgorithm: string;
    canonicalizationMethod: string;
}

interface SigningCertificate {
    encodedCertificate: CertificateResponse | string; // defined earlier
}

interface BlevelParams {
    trustAnchorBPPolicy: boolean;
    signingDate: number;
}

interface Parameters {
    signWithExpiredCertificate: boolean;
    signatureLevel: string;
    signaturePackaging: string;
    signatureAlgorithm: string;
    encryptionAlgorithm: string;
    digestAlgorithm: string;
    referenceDigestAlgorithm?: any;
    maskGenerationFunction?: any;
    contentTimestampParameters: ContentTimestampParameters;
    signatureTimestampParameters: SignatureTimestampParameters;
    archiveTimestampParameters: ArchiveTimestampParameters;
    signingCertificate: SigningCertificate;
    certificateChain: CertificateChain[];
    detachedContents?: any;
    asicContainerType?: any;
    blevelParams: BlevelParams;
}

interface CertificateChain {
    encodedCertificate: string;
}

interface ToSignDocument {
    bytes: string;
    digestAlgorithm?: any;
    name: string;
}

export interface GetDataRequest {
    parameters: Parameters;
    toSignDocument: ToSignDocument;
}

export interface GetDataResponse {
    bytes: string;
}