/**
 * Type declaration for validation request JSON root object
 * Important fields: SignedDocument.bytes
 */
export interface ValidationRequest {
    signedDocument: SignedDocument;
    policy?: any;
    signatureId?: any;
}

export interface Conclusion {
    Indication: string;
    SubIndication: string;
    Errors: Error2[];
    Warnings: any[];
    Infos: any[];
}

interface SignedDocument {
    bytes: string;
    digestAlgorithm?: any;
    name: string;
}

/**
 * Type declaration for validation response JSON root object
 */
export interface ValidationReport {
    validationReportaDataHandler: string;
    DiagnosticData: DiagnosticData;
    SimpleReport: SimpleReport;
    DetailedReport: DetailedReport;
}

interface StructuralValidation {
    Valid: boolean;
    Message?: any;
}

interface BasicSignature {
    EncryptionAlgoUsedToSignThisToken: string;
    KeyLengthUsedToSignThisToken: string;
    DigestAlgoUsedToSignThisToken: string;
    MaskGenerationFunctionUsedToSignThisToken?: any;
    SignatureIntact: boolean;
    SignatureValid: boolean;
}

/**
 * Other type declarations for validation response JSON
 */

interface DigestMatcher {
    DataFound: boolean;
    DataIntact: boolean;
    DigestMethod: string;
    DigestValue: string;
    type: string;
    name: string;
}

interface SigningCertificate {
    AttributePresent: boolean;
    DigestValuePresent: boolean;
    DigestValueMatch: boolean;
    IssuerSerialMatch: boolean;
    PublicKey?: any;
    Certificate: string;
}

interface ChainItem {
    Certificate: string;
}

interface Policy {
    Id: string;
    Url?: any;
    Description?: any;
    Notice?: any;
    ZeroHash: boolean;
    DigestAlgoAndValue?: any;
    Asn1Processable: boolean;
    Identified: boolean;
    Status: boolean;
    ProcessingError?: any;
    DigestAlgorithmsEqual: boolean;
}

interface SignerDocumentRepresentations {
    HashOnly: boolean;
    DocHashOnly: boolean;
}

interface DigestAlgoAndValue {
    DigestMethod: string;
    DigestValue: string;
}

interface CertificateRef {
    Origin: string;
    IssuerSerial: string;
    DigestAlgoAndValue: DigestAlgoAndValue;
}

interface RelatedCertificate {
    Origin: string[];
    CertificateRef: CertificateRef[];
    Certificate: string;
}

interface FoundCertificates {
    RelatedCertificate: RelatedCertificate[];
    OrphanCertificate: any[];
}

interface FoundRevocations {
    RelatedRevocation: any[];
    OrphanRevocation: any[];
}

interface SignatureScope {
    Scope: string;
    Name: string;
    Description: string;
    Transformation: string[];
    SignerData: string;
}

interface SignatureDigestReference {
    CanonicalizationMethod: string;
    DigestMethod: string;
    DigestValue: string;
}

interface Signature {
    Id: string;
    DAIdentifier: string;
    SignatureFilename: string;
    ErrorMessage?: any;
    ClaimedSigningTime: Date;
    SignatureFormat: string;
    StructuralValidation: StructuralValidation;
    DigestMatcher: DigestMatcher[];
    BasicSignature: BasicSignature;
    SigningCertificate: SigningCertificate;
    ChainItem: ChainItem[];
    ContentType?: any;
    MimeType: string;
    ContentIdentifier?: any;
    ContentHints?: any;
    SignatureProductionPlace?: any;
    Indication: any[];
    SignerRole: any[];
    Policy: Policy;
    PDFRevision?: any;
    SignerDocumentRepresentations: SignerDocumentRepresentations;
    FoundCertificates: FoundCertificates;
    FoundRevocations: FoundRevocations;
    FoundTimestamp: any[];
    SignatureScope: SignatureScope[];
    SignatureDigestReference: SignatureDigestReference;
    SignatureValue: string;
    CounterSignature?: any;
    Parent?: any;
    Duplicated?: any;
}

interface SubjectDistinguishedName {
    value: string;
    Format: string;
}

interface IssuerDistinguishedName {
    value: string;
    Format: string;
}

interface BasicSignature2 {
    EncryptionAlgoUsedToSignThisToken: string;
    KeyLengthUsedToSignThisToken: string;
    DigestAlgoUsedToSignThisToken: string;
    MaskGenerationFunctionUsedToSignThisToken?: any;
    SignatureIntact?: any;
    SignatureValid?: any;
}

interface DigestAlgoAndValue2 {
    DigestMethod: string;
    DigestValue: string;
}

interface Certificate {
    Id: string;
    SubjectDistinguishedName: SubjectDistinguishedName[];
    IssuerDistinguishedName: IssuerDistinguishedName[];
    SerialNumber: number;
    CommonName: string;
    Locality?: any;
    State?: any;
    CountryName?: any;
    OrganizationName: string;
    GivenName?: any;
    OrganizationalUnit?: any;
    Surname?: any;
    Pseudonym?: any;
    Email?: any;
    aiaUrl: any[];
    crlUrl: any[];
    ocspServerUrl: any[];
    Source: string[];
    NotAfter: Date;
    NotBefore: Date;
    PublicKeySize: number;
    PublicKeyEncryptionAlgo: string;
    KeyUsage: string[];
    extendedKeyUsagesOid: any[];
    IdPkixOcspNoCheck: boolean;
    BasicSignature: BasicSignature2;
    SigningCertificate?: any;
    ChainItem: any[];
    Trusted: boolean;
    SelfSigned: boolean;
    certificatePolicy: any[];
    qcStatementOid: any[];
    qcTypeOid: any[];
    TrustedServiceProvider: any[];
    CertificateRevocation: any[];
    Base64Encoded?: any;
    DigestAlgoAndValue: DigestAlgoAndValue2;
}

interface DigestAlgoAndValue3 {
    DigestMethod: string;
    DigestValue: string;
}

interface SignerData {
    Id: string;
    ReferencedName: string;
    DigestAlgoAndValue: DigestAlgoAndValue3;
}

interface DiagnosticData {
    DocumentName: string;
    ValidationDate: Date;
    ContainerInfo?: any;
    Signature: Signature[];
    Certificate: Certificate[];
    Revocation: any[];
    Timestamp: any[];
    OrphanToken: any[];
    SignerData: SignerData[];
    TrustedList: any[];
}

interface ValidationPolicy {
    PolicyName: string;
    PolicyDescription: string;
}

interface SignatureLevel {
    value: string;
    description: string;
}

interface SignatureScope2 {
    value: string;
    name: string;
    scope: string;
}

interface Certificate2 {
    id: string;
    qualifiedName: string;
}

interface CertificateChain {
    Certificate: Certificate2[];
}

interface Signature2 {
    SigningTime: Date;
    BestSignatureTime: Date;
    SignedBy: string;
    SignatureLevel: SignatureLevel;
    SignatureScope: SignatureScope2[];
    Filename?: any;
    CertificateChain: CertificateChain;
    Indication: string;
    SubIndication: string;
    Errors: string[];
    Warnings: string[];
    Infos: any[];
    Id: string;
    CounterSignature?: any;
    ParentId?: any;
    SignatureFormat: string;
}

interface SignatureOrTimestamp {
    Signature: Signature2;
}

interface SimpleReport {
    ValidationPolicy: ValidationPolicy;
    ValidationTime: Date;
    DocumentName: string;
    ValidSignaturesCount: number;
    SignaturesCount: number;
    ContainerType?: any;
    signatureOrTimestamp: SignatureOrTimestamp[];
}

interface Name {
    value: string;
    NameId: string;
}

interface Error {
    value: string;
    NameId: string;
}

interface Constraint {
    Name: Name;
    Status: string;
    Error: Error;
    Warning?: any;
    Info?: any;
    AdditionalInfo?: any;
    Id: string;
}

interface Error2 {
    value: string;
    NameId: string;
}

interface ProofOfExistence {
    Time: Date;
    TimestampId?: any;
}

interface ValidationProcessBasicSignature {
    Constraint: Constraint[];
    Conclusion: Conclusion;
    Title: string;
    ProofOfExistence: ProofOfExistence;
}

interface Name2 {
    value: string;
    NameId: string;
}

interface Error3 {
    value: string;
    NameId: string;
}

interface Constraint2 {
    Name: Name2;
    Status: string;
    Error: Error3;
    Warning?: any;
    Info?: any;
    AdditionalInfo?: any;
    Id?: any;
}

interface Error4 {
    value: string;
    NameId: string;
}

interface Conclusion2 {
    Indication: string;
    SubIndication: string;
    Errors: Error4[];
    Warnings: any[];
    Infos: any[];
}

interface ProofOfExistence2 {
    Time: Date;
    TimestampId?: any;
}

interface ValidationProcessLongTermData {
    Constraint: Constraint2[];
    Conclusion: Conclusion2;
    Title: string;
    ProofOfExistence: ProofOfExistence2;
}

interface Name3 {
    value: string;
    NameId: string;
}

interface Error5 {
    value: string;
    NameId: string;
}

interface Constraint3 {
    Name: Name3;
    Status: string;
    Error: Error5;
    Warning?: any;
    Info?: any;
    AdditionalInfo?: any;
    Id?: any;
}

interface Error6 {
    value: string;
    NameId: string;
}

interface Conclusion3 {
    Indication: string;
    SubIndication: string;
    Errors: Error6[];
    Warnings: any[];
    Infos: any[];
}

interface ProofOfExistence3 {
    Time: Date;
    TimestampId?: any;
}

interface ValidationProcessArchivalData {
    Constraint: Constraint3[];
    Conclusion: Conclusion3;
    Title: string;
    ProofOfExistence: ProofOfExistence3;
}

interface Name4 {
    value: string;
    NameId: string;
}

interface Error7 {
    value: string;
    NameId: string;
}

interface Warning {
    value: string;
    NameId: string;
}

interface Constraint4 {
    Name: Name4;
    Status: string;
    Error: Error7;
    Warning: Warning;
    Info?: any;
    AdditionalInfo?: any;
    Id?: any;
}

interface Error8 {
    value: string;
    NameId: string;
}

interface Warning2 {
    value: string;
    NameId: string;
}

interface Conclusion4 {
    Indication: string;
    SubIndication?: any;
    Errors: Error8[];
    Warnings: Warning2[];
    Infos: any[];
}

interface ValidationSignatureQualification {
    ValidationCertificateQualification: any[];
    Constraint: Constraint4[];
    Conclusion: Conclusion4;
    Title: string;
    Id: string;
    SignatureQualification: string;
}

interface Signature3 {
    ValidationProcessBasicSignature: ValidationProcessBasicSignature;
    Timestamp: any[];
    ValidationProcessLongTermData: ValidationProcessLongTermData;
    ValidationProcessArchivalData: ValidationProcessArchivalData;
    ValidationSignatureQualification: ValidationSignatureQualification;
    ValidationTimestampQualification?: any;
    Id: string;
    CounterSignature?: any;
}

interface SignatureOrTimestampOrCertificate {
    Signature: Signature3;
}

interface Name5 {
    value: string;
    NameId: string;
}

interface Constraint5 {
    Name: Name5;
    Status: string;
    Error?: any;
    Warning?: any;
    Info?: any;
    AdditionalInfo?: any;
    Id?: any;
}

interface Conclusion5 {
    Indication: string;
    SubIndication?: any;
    Errors?: any;
    Warnings: any[];
    Infos: any[];
}

interface FC {
    Constraint: Constraint5[];
    Conclusion: Conclusion5;
    Title: string;
}

interface ChainItem2 {
    Source: string;
    Id: string;
}

interface CertificateChain2 {
    ChainItem: ChainItem2[];
}

interface Name6 {
    value: string;
    NameId: string;
}

interface Constraint6 {
    Name: Name6;
    Status: string;
    Error?: any;
    Warning?: any;
    Info?: any;
    AdditionalInfo?: any;
    Id?: any;
}

interface Conclusion6 {
    Indication: string;
    SubIndication?: any;
    Errors?: any;
    Warnings: any[];
    Infos: any[];
}

interface ISC {
    CertificateChain: CertificateChain2;
    Constraint: Constraint6[];
    Conclusion: Conclusion6;
    Title: string;
}

interface Name7 {
    value: string;
    NameId: string;
}

interface Constraint7 {
    Name: Name7;
    Status: string;
    Error?: any;
    Warning?: any;
    Info?: any;
    AdditionalInfo?: any;
    Id?: any;
}

interface Conclusion7 {
    Indication: string;
    SubIndication?: any;
    Errors?: any;
    Warnings: any[];
    Infos: any[];
}

interface VCI {
    Constraint: Constraint7[];
    Conclusion: Conclusion7;
    Title: string;
}

interface Name8 {
    value: string;
    NameId: string;
}

interface Error9 {
    value: string;
    NameId: string;
}

interface Constraint8 {
    Name: Name8;
    Status: string;
    Error: Error9;
    Warning?: any;
    Info?: any;
    AdditionalInfo?: any;
    Id?: any;
}

interface Error10 {
    value: string;
    NameId: string;
}

interface Conclusion8 {
    Indication: string;
    SubIndication: string;
    Errors: Error10[];
    Warnings: any[];
    Infos: any[];
}

interface XCV {
    SubXCV: any[];
    Constraint: Constraint8[];
    Conclusion: Conclusion8;
    Title: string;
}

interface Name9 {
    value: string;
    NameId: string;
}

interface Constraint9 {
    Name: Name9;
    Status: string;
    Error?: any;
    Warning?: any;
    Info?: any;
    AdditionalInfo: string;
    Id?: any;
}

interface Conclusion9 {
    Indication: string;
    SubIndication?: any;
    Errors?: any;
    Warnings: any[];
    Infos: any[];
}

interface CV {
    Constraint: Constraint9[];
    Conclusion: Conclusion9;
    Title: string;
}

interface CryptographicInfo {
    Algorithm: string;
    KeyLength: string;
    Secure: boolean;
    NotAfter: Date;
}

interface Name10 {
    value: string;
    NameId: string;
}

interface Constraint10 {
    Name: Name10;
    Status: string;
    Error?: any;
    Warning?: any;
    Info?: any;
    AdditionalInfo: string;
    Id?: any;
}

interface Conclusion10 {
    Indication: string;
    SubIndication?: any;
    Errors?: any;
    Warnings: any[];
    Infos: any[];
}

interface SAV {
    CryptographicInfo: CryptographicInfo;
    Constraint: Constraint10[];
    Conclusion: Conclusion10;
    Title: string;
    ValidationTime: Date;
}

interface ChainItem3 {
    Source: string;
    Id: string;
}

interface CertificateChain3 {
    ChainItem: ChainItem3[];
}

interface Error11 {
    value: string;
    NameId: string;
}

interface Conclusion11 {
    Indication: string;
    SubIndication: string;
    Errors: Error11[];
    Warnings?: any;
    Infos?: any;
}

interface BasicBuildingBlock {
    FC: FC;
    ISC: ISC;
    VCI: VCI;
    XCV: XCV;
    CV: CV;
    SAV: SAV;
    PSV?: any;
    PCV?: any;
    VTS?: any;
    CertificateChain: CertificateChain3;
    Conclusion: Conclusion11;
    Id: string;
    Type: string;
}

interface DetailedReport {
    signatureOrTimestampOrCertificate: SignatureOrTimestampOrCertificate[];
    BasicBuildingBlocks: BasicBuildingBlock[];
    TLAnalysis: any[];
}