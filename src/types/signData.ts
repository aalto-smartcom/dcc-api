
import { CertificateResponse, DigestResponse } from './types'

/***
 * Sign document
*/

interface SigningCertificate {
    encodedCertificate: CertificateResponse | string; // defined earlier
}

interface ContentTimestampParameters {
    digestAlgorithm: string;
    canonicalizationMethod: string;
}

interface SignatureTimestampParameters {
    digestAlgorithm: string;
    canonicalizationMethod: string;
}

interface ArchiveTimestampParameters {
    digestAlgorithm: string;
    canonicalizationMethod: string;
}

interface BlevelParams {
    trustAnchorBPPolicy: boolean;
    signingDate: number;
}

interface Parameters {
    signingCertificate: SigningCertificate;
    certificateChain: CertificateChain[];
    detachedContents?: any;
    asicContainerType?: any;
    signatureLevel: string;
    signaturePackaging: string;
    signatureAlgorithm: string;
    digestAlgorithm: string;
    encryptionAlgorithm: string;
    referenceDigestAlgorithm?: any;
    contentTimestampParameters: ContentTimestampParameters;
    signatureTimestampParameters: SignatureTimestampParameters;
    archiveTimestampParameters: ArchiveTimestampParameters;
    signWithExpiredCertificate: boolean;
    blevelParams: BlevelParams;
}

interface CertificateChain {
    encodedCertificate: string;
}

interface SignatureValue {
    algorithm: string;
    value: DigestResponse | string;
}

interface ToSignDocument {
    bytes: string;
    digestAlgorithm?: any;
    name: string;
}

export interface SignRequest {
    parameters: Parameters;
    signatureValue: SignatureValue;
    toSignDocument: ToSignDocument;
}


/***
 * GetDataResponse
 */
export interface SignResponse {
    bytes: string;
    digestAlgorithm?: any;
    name: string;
}