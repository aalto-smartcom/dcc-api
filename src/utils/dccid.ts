import crypto from 'crypto'

const config = require('./config')
const companyId = config.COMPANY_ID
const dccVersion = config.DCC_VERSION

export const URIPattern = (
  /(\r\n|\r|\n)?<dcc:cryptographicIdentifier>(.*)<\/dcc:cryptographicIdentifier>(\r\n|\r|\n)?/gi
)

/**
 * Generates a URL-safe-Base64-encoded SHA256 fingerprint of the input string
 * @param str: string
 */
export const generateFingerprint = async (str: string): Promise<string> => (

  crypto.createHash('sha256').update(str).digest('base64')
  /* Base64 -> URL-safe Base64 */
    .replace(/\+/g, '-')    // Replace + with -
    .replace(/\//g, '_')    // Replace / with _
    .replace( /=/g,  '')    // Remove padding
)
  
  
/**
 * Generates an URI containing the DCC ID
 * Example:
 * dcc://952000001.230/v5HLu6rS5FU4lfp-/83N_4FW4CZqz2pj60DN-mwKfhc4
 *
 * @param companyId GDTI CompanyPrefix
 * @param dccVersion GDTI DocumentType (DCC version)
 * @param fingerprint URL-safe Base64-encoded SHA256 fingerprint of the DCC document
 * */
const generateIdentifier = async (companyId: string, dccVersion: string, fingerprint: string): Promise<string> => (

  `dcc://${companyId}.${dccVersion}/${fingerprint.substring(0, 16)}/${fingerprint.substring(16)}`
)

  
/**
 * Inserts the DCC ID URI between </ds:Signature> and </dcc:digitalCalibrationCertificate>
 * Throws an error if signature is not found
 * @param xml
 */
export const insertIdentifier = async (xml: string): Promise<string> => {
  if (!xml.includes('</ds:Signature>')) {
    throw new Error('Failed to insert ID due to missing signature')
  }
  
  const [startTag, endTag] = ['<dcc:cryptographicIdentifier>', '</dcc:cryptographicIdentifier>']
  const fingerprint = await generateFingerprint(xml)

  return xml.replace(
    '</ds:Signature>',
    `</ds:Signature>\n${startTag + await generateIdentifier(companyId, dccVersion, fingerprint) + endTag}\n`
  )
}
  
  
/**
 * Removes the DCC ID based on pattern matching
 * Does nothing if ID is not found
 * @param xml
 */
export const removeIdentifier = async (xml: string): Promise<string> => xml.replace(URIPattern, '')