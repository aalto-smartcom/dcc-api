/* eslint-disable quotes */
/* eslint-disable no-undef */

const unknownEndpoint = (request, response) => {
  response.status(404).json(
    { error: 'Unknown endpoint' }
  )
}

// eslint-disable-next-line no-unused-vars
const errorHandler = (error, request, response, _next) => {
  console.log(error.message)

  switch (error.name) {
  case 'TypeError':
    if (error.message.includes(`Cannot read property 'certificate' of undefined`)) {
      return response.status(400).json(
        { 'error' : 'Invalid signeralias provided in the request.' }
      )
    } else {
      return response.status(400).json(
        { 'error': error.message }
      )
    }
  case 'ValidationError':
    return response.status(400).json(
      { 'error': 'Input XML failed to validate against the DCC schema' }
    )
  case 'WriteOutputError':
    return response.status(503).json(
      { 'error': 'Error writing output while validating XML' }
    )
  case 'OutOfMemoryError':
    return response.status(503).json(
      { 'error': 'Ran out of memory while parsing XML' }
    )
  case 'ConnectionError':
    return response.status(503).json(
      { 'error': 'Error connecting to eIDAS service' }
    )
  case 'SyntaxError':
    return response.status(400).json(
      { 'error': `Syntax Error while parsing your request: ${error.message}`}
    )
  default:
    if (error.message.includes('parser error')) {
      return response.status(400).json(
        { 'error': `Failed to parse input XML:\n${error.message}` }
      )
    } else if (error.toString().includes('ERR_INVALID_ARG_TYPE')) {
      return response.status(400).json(
        { 'error': `Failed to parse request:\n${error}` }
      )
    } else {
      return response.status(500).json(
        { 'error': `An error occurred while processing your request: ${error}` }
      )
    }
  }
}

module.exports = {
  unknownEndpoint,
  errorHandler
}