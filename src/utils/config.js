require('dotenv').config()
/**
 *  DCC configurations
 *  
 *  COMPANY_ID: GDTI CompanyPrefix, defaults to demo ID 952000000
 */
let COMPANY_ID = process.env.COMPANY_ID || '952000000'
let DCC_VERSION = process.env.DCC_VERSION || '230'

/**
 *  Environment variables
 */
let PORT = process.env.PORT || 3000 // Port for INTERNAL application usage.
let EIDAS_URI = process.env.EIDAS_URI || 'http://eidas-rest:8080' // Defaults to docker network name.
let XML_SCHEMA_PATH = process.env.XML_SCHEMA_PATH || './templates/schema/schema.xsd' // PTB DSI Schema.
let CANONICALIZATION_METHOD = process.env.CANONICALIZATION_METHOD || 'exc-c14n' // Default c14n method.
let SIGNERALIAS = process.env.SIGNERALIAS // From signer keystore alias.
let CORSALLOW = process.env.CORSALLOW 

if (process.env.NODE_ENV === 'development') {
  EIDAS_URI = process.env.DEVELOPMENT_EIDAS_URI || 'http://localhost:8080' // For running in localhost mode.
  SIGNERALIAS = 'testcert'
}

if (process.env.NODE_ENV === 'test') {
  EIDAS_URI = process.env.TEST_EIDAS_URI || 'http://eidas-rest:8080' // Running in docker network mode.
  PORT = process.env.TEST_PORT || 3001
  SIGNERALIAS = 'testcert'
}

module.exports = {
  COMPANY_ID,
  DCC_VERSION,
  CANONICALIZATION_METHOD,
  XML_SCHEMA_PATH,
  EIDAS_URI,
  PORT,
  SIGNERALIAS,
  CORSALLOW
}
