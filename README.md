# DCC-Toolkit

## Attention

Since the version 0.0.2 this repository uses a proprietary eIDAS implementation. The quickstart will still work perfectly fine, but if you need to use your own .p12 keystores you will need to compile [0.0.1](https://gitlab.com/aalto-smartcom/dcc-api/-/tags) version which does not support **verifying againts a trusted chain**. 

### Using the API

Please refer to the [wiki page](https://gitlab.com/aalto-smartcom/dcc-api/-/wikis/home#using-the-api) for instructions on signing and validating DCCs with the help of Postman examples.

### Quickstart

1. Create a file called ```docker-compose.yml``` with the following contects.
```yml
version: '3.5'

services:
  eidas-rest:
    image: registry.gitlab.com/aalto-smartcom/dss-demonstrations/eidas-rest
    ports:
      - 8080:8080

  signing-api:
    restart: always
    image: registry.gitlab.com/aalto-smartcom/dcc-api/signing-api:0.0.1
    ports:
    - 80:3000
    depends_on:
      - "eidas-rest"

networks:
    default:
      name: stamping-server
      driver: bridge
```

2. Bring the stack up:
```
docker-compose up
```
3. Import example collection to Postman from [here](https://gitlab.com/aalto-smartcom/dcc-api/-/blob/master/documentation/postman/signingAPI_postman.json) instruction can be found [in the wiki](https://gitlab.com/aalto-smartcom/dcc-api/-/wikis/home#using-the-api).


#### Want to build from source?

You will need to **clone** the repo and the **propiertary** eidas-server into this folder. Please refer to the repo's [./docker-compose.yml](Docker-compose file). Also make sure you have generated a PKI and placed them in the correct _places_.
_This will take a long time_

### Developing

Please make sure you have node.js and npm installed before starting. Clone this repository, open the folder in terminal and run `npm install` to install the required dependencies. To start the service in development mode, run `npm run dev` and in production mode run `npm start`.

### Unit tests locally

If you have logged in to the GitLab registry you can run ```docker-compose run signing-api npm run test```.

To run unit tests (when running eidas-rest at the default 8080 port locally):  
```docker build -t signing-api . && docker run --network stamping-server -e CI=true signing-api npm run test```

### Logging

Logging has been moved to [https://gitlab.com/aalto-smartcom/elastic-logging](https://gitlab.com/aalto-smartcom/elastic-logging)
If you want to start the logging service (Elastic Stack):
Navigate to the `elastic-logging` folder and run:
```
docker-compose up
```

## Used ports

The project uses following ports:

```
signingAPI      80, 443 (3000 in Docker)
eIDAS REST      8080

Logging:
Logstash        5000
Logstash        9600
```

# Enviroment configuration

```
#REQUIRED - From signer keystore alias.
SIGNERALIAS=

# Optional
EIDAS_URI=
PORT=
XML_SCHEMA_PATH=
CANONICALIZATION_METHOD=
COMPANY_ID=
DCC_VERSION=
```

# SAML integration

Generating the service providers signing and decryption keys for example.  
```openssl req -x509 -newkey rsa:4096 -keyout signing_key.pem -out signing_cert.pem -nodes -days 900```  
```openssl req -x509 -newkey rsa:4096 -keyout decryption_key.pem -out decryption_cert.pem -nodes -days 900```  
and move the files into src/cert