FROM node:12-alpine
WORKDIR /usr/src/app
RUN apk update && apk add libxml2 libxml2-utils
COPY package*.json ./
RUN npm ci
COPY . .
EXPOSE 3003
CMD [ "npm", "start" ]